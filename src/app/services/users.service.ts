import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public users: any;
  public apiUrl = 'https://randomuser.me/api/';

  constructor(private httpClient: HttpClient) {
  }

  getUsers(): Observable<User[]> {
    let headers = new HttpHeaders();
    let params: HttpParams = new HttpParams();
    params = params.set('results', '50');

    return this.httpClient.get(this.apiUrl, {headers, params}).pipe(map(data => {
      let usersList = data['results'];
      return usersList.map(function (user: any) {
        user.id = user.login.uuid;
        return user;
      });
    }));
  }
  getUser(userId: string): Observable<User>{
    let headers = new HttpHeaders();
    let params: HttpParams = new HttpParams();
    params = params.set('uuid', userId);

    return this.httpClient.get(this.apiUrl, {headers, params}).pipe(map(data => {
      let user = data['results'][0];
      user.id = user.login.uuid;
      return user;
    }));
  }
}
