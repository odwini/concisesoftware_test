import { Action } from '@ngrx/store';
import { User } from '../models/user';

export const GET =           '[User] Get';
export const GET_COMPLETE =  '[User] Get Complete';
export const LOAD =             '[User] Load';
export const SELECT =           '[User] Select';


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class GetAction implements Action {
  readonly type = GET;

  constructor(public payload: string) { }
}

export class GetCompleteAction implements Action {
  readonly type = GET_COMPLETE;

  constructor(public payload: User[]) { }
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: User) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions
  = GetAction
  | GetCompleteAction
  | LoadAction
  | SelectAction;
