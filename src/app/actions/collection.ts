import { Action } from '@ngrx/store';
import {User} from '../models/user';


export const ADD_USER =             '[Collection] Add User';
export const ADD_USER_SUCCESS =     '[Collection] Add User Success';
export const ADD_USER_FAIL =        '[Collection] Add User Fail';
export const REMOVE_USER =          '[Collection] Remove User';
export const REMOVE_USER_SUCCESS =  '[Collection] Remove User Success';
export const REMOVE_USER_FAIL =     '[Collection] Remove User Fail';
export const LOAD =                 '[Collection] Load';
export const LOAD_SUCCESS =         '[Collection] Load Success';
export const LOAD_FAIL =            '[Collection] Load Fail';


/**
 * Add User to Collection Actions
 */
export class AddUserAction implements Action {
  readonly type = ADD_USER;

  constructor(public payload: User) { }
}

export class AddUserSuccessAction implements Action {
  readonly type = ADD_USER_SUCCESS;

  constructor(public payload: User) { }
}

export class AddUserFailAction implements Action {
  readonly type = ADD_USER_FAIL;

  constructor(public payload: User) { }
}


/**
 * Remove User from Collection Actions
 */
export class RemoveUserAction implements Action {
  readonly type = REMOVE_USER;

  constructor(public payload: User) { }
}

export class RemoveUserSuccessAction implements Action {
  readonly type = REMOVE_USER_SUCCESS;

  constructor(public payload: User) { }
}

export class RemoveUserFailAction implements Action {
  readonly type = REMOVE_USER_FAIL;

  constructor(public payload: User) {}
}

/**
 * Load Collection Actions
 */
export class LoadAction implements Action {
  readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: User[]) { }
}

export class LoadFailAction implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}


export type Actions
  = AddUserAction
  | AddUserSuccessAction
  | AddUserFailAction
  | RemoveUserAction
  | RemoveUserSuccessAction
  | RemoveUserFailAction
  | LoadAction
  | LoadSuccessAction
  | LoadFailAction;
