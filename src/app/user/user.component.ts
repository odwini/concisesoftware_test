import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  private uuid: string;
  private sub: any;
  public user: any;

  constructor(private route: ActivatedRoute, private userService: UsersService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.uuid = params.uuid;
      this.user = this.getUser();
    });
  }

  getUser(){
    let subscription = this.userService.getUser(this.uuid).subscribe(
      data => {
        this.user = data;
        console.log(this.user);
        subscription.unsubscribe();
      },
      err => console.error(err)
    );
  }
}
