import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public users: any;
  constructor(public usersService: UsersService) { }

  ngOnInit() {
    this.users = this.getUsers();
  }

  getUsers(){
    let subscription = this.usersService.getUsers().subscribe(
      data => {
        this.users = data;
        subscription.unsubscribe();
      },
      err => console.error(err)
    );
  }
}
