export interface User {
  id: string;
  cell: string;
  dob: {
    age: number;
    date: string;
  };
  email: string;
  gender: string;
  location: {
    city: string;
    coordinates: {
      latitude: string;
      longitude: string;
    }
  };
  picture: {
    large: string
  };
  uuid: string;
  name: {
    first: string;
    last: string;
    title: string;
  };
}
