import { createSelector } from 'reselect';
import { User } from '../models/user';
import * as user from '../actions/user';
import * as collection from '../actions/collection';


export interface State {
  ids: string[];
  entities: { [id: string]: User };
  selectedUserId: string | null;
};

export const initialState: State = {
  ids: [],
  entities: {},
  selectedUserId: null,
};

export function reducer(state = initialState, action: user.Actions | collection.Actions): State {
  switch (action.type) {
    case user.GET_COMPLETE:
    case collection.LOAD_SUCCESS: {
      const users = action.payload;
      const newUsers = users.filter(user => !state.entities[user.id]);

      const newUserIds = newUsers.map(user => user.id);
      const newUserEntities = newUsers.reduce((entities: { [id: string]: User }, user: User) => {
        return Object.assign(entities, {
          [user.id]: user
        });
      }, {});

      return {
        ids: [ ...state.ids, ...newUserIds ],
        entities: Object.assign({}, state.entities, newUserEntities),
        selectedUserId: state.selectedUserId
      };
    }

    case user.LOAD: {
      const user = action.payload;

      if (state.ids.indexOf(user.id) > -1) {
        return state;
      }

      return {
        ids: [ ...state.ids, user.id ],
        entities: Object.assign({}, state.entities, {
          [user.id]: user
        }),
        selectedUserId: state.selectedUserId
      };
    }

    case user.SELECT: {
      return {
        ids: state.ids,
        entities: state.entities,
        selectedUserId: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */

export const getEntities = (state: State) => state.entities;

export const getIds = (state: State) => state.ids;

export const getSelectedId = (state: State) => state.selectedUserId;

export const getSelected = createSelector(getEntities, getSelectedId, (entities, selectedId) => {
  return entities[selectedId];
});

export const getAll = createSelector(getEntities, getIds, (entities, ids) => {
  return ids.map(id => entities[id]);
});
